FROM debian:#sed#debian_tag

RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# enable contib sources
RUN sed -i 's/ main/ main contrib/' /etc/apt/sources.list

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install --assume-yes \
	apt-utils \
	gnupg2 \
""
