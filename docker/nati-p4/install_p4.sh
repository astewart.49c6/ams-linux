#!/usr/bin/env bash
# This script installs the p4 CLI utility on linux docker images.
set -xeuo pipefail

download_and_verify () {
	local url="${1}"
	local shasum_path="${2:-}"

	wget --no-verbose "${url}"

	if [ -n "$shasum_path" ]; then
		wget --no-verbose "${shasum_path}" -O SHA256SUMS
		sha256sum --strict --ignore-missing --check SHA256SUMS
	fi
}

install_p4_cli () {
	local p4_cli_release="r20.1"
	local p4_cli_url_base="http://www.perforce.com/downloads/perforce/${p4_cli_release}/bin.linux26x86_64"
	local p4_cli_url="${p4_cli_url_base}/helix-core-server.tgz"
	local p4_cli_shasum_url="${p4_cli_url_base}/SHA256SUMS"
	
	download_and_verify "${p4_cli_url}" "${p4_cli_shasum_url}"

	tar -xz -f helix-core-server.tgz --to-stdout 'p4' >/usr/bin/p4
	chmod 0755 /usr/bin/p4
}

install_p4v () {
	local p4v_release="r20.1"
	local p4v_url_base="http://www.perforce.com/downloads/perforce/${p4v_release}/bin.linux26x86_64"
	local p4v_url="${p4v_url_base}/p4v.tgz"
	local p4v_shasum_url="${p4v_url_base}/SHA256SUMS"
	
	download_and_verify "${p4v_url}" "${p4v_shasum_url}"

	tar -xzf p4v.tgz
	local OLDPWD="${PWD}"
	cd p4v-*
	chown -R root:root ./*
	cp -vr ./bin/* /bin
	cp -vr ./lib/* /lib
	cd "$OLDPWD"
}

tmp_p4=`mktemp -d`
cd "$tmp_p4"

install_p4_cli
install_p4v

rm -rv "$tmp_p4"
